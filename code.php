<?php

// [SECTION] Repetition Control Structures

// While Loop 

function whileLoop() {
	$count = 5;

	while($count !== 0) {
		echo $count . "</br>";
		$count--;
	}
}

function doWhileLoop() {
	$count = 10;

	do {
		echo $count . "</br>";
		$count--;
	} while ($count !== 0);
}

function forLoop() {
	for ($count = 0; $count <= 10; $count++) {
		echo $count . "<br>";
	}
}

// Continue and Break Statement
/*
	"Continue" keyword allows the code to go to the next loop without finishing the current code block.
	"Break" keyword is used to stop the execution of the current loop.
*/

function modifiedForLoop(){
	for($count = 0; $count <= 20; $count++){
		if($count % 2 === 0){
			continue;
		}
		echo $count . "<br>";
		if($count >= 10) {
			break;
		}
	}
}

// [SECTION] Array Manipulation
// An array is a kind of variable that can hold more than one value.
// Arrays in php are declared using array() function or square brackets "[]"

$studentNumber = array("2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"); // before PHP 5.4

$studentNumber = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"]; // introduced on PHP 5.4

//Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
$tasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake react"
];

// Associative Array
// Associative array differs from the numeric array in the sense that associative array uses descriptive names in naming the element values (key=>value pair)
// double array operator (=>) is an assignment operator that is commonly used in the creation of associative array.

$gradesPeriods = ["firstGrading" => 98.5, "secondGrading" => 94.3, "thirdGrading" => 89.2, "fourthGrading" => 90.1];

// Two-Dimensional Array
$heroes = [
	["iron man", "thor", "hulk"],
	["wolverine", "cyclops", "jean grey"],
	["batman", "superman", "wonderwoman"]
];

// Two-Dimensional Associative Array
$ironManPowers = [
	"regular" => ["repulsor blast", "rocket punch"],
	"signature" => ["unibeam"]
];

// Array Methods
// "Array mutations" seek to modify the contents of an array while "Array Iterations" which aims to evaluate each element in the array

// Array sorting
// sorting modifies the array itself
$sortedBrands = $computerBrands;
$reversedSortedBrands = $computerBrands;

// Ascending order
sort($sortedBrands);
// Descending order
rsort($reversedSortedBrands);

// in_array() function

function searchBrand($brands, $brand) {
	// in_array($searchValue, $arrayList)
	// return is Boolean (true/false)
	return(in_array($brand, $brands)) ? "$brand is in the array" : "$brand is not in the array.";
}

// array_reverse()
// Reversing the arrangement of the elements do not change the array itself.
$reversedGradesPeriods = array_reverse($gradesPeriods);